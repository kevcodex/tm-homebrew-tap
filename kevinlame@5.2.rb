# Documentation: https://docs.brew.sh/Formula-Cookbook
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class KevinlameAT52 < Formula
  desc ""
  homepage "https://bitbucket.org/kevcodex"
  url "https://bitbucket.org/kevcodex/teammergeautomation/raw/5b98def4715eadefae6752e5925c44c6c13b55cf/releases/kevinlame.tar.gz"
  version "5.2"
  sha256 "7c684a67436544cd8bfb72e7ce20b761a2c1720d2611f0ee4c0c0fc5d1b7a2d0"
  # depends_on "cmake" => :build

  def install
    bin.install "KevinLame"
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test KevinLame`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
