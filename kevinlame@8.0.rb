# Documentation: https://docs.brew.sh/Formula-Cookbook
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class KevinlameAT80 < Formula
  desc ""
  homepage "https://bitbucket.phunware.com/projects/SERVICES/repos/dev-team-merge-automator/browse"
  url "https://www.dropbox.com/s/88ynv5ywc4oq1q0/8.0.tar.gz?dl=1"
  version "8.0"
  sha256 "f41c5770031da0e7cfc4fe39bdbdd3d7dfbf2ff4f3abcafe317ea95471c1062e"
  # depends_on "cmake" => :build

  def install
    bin.install "KevinLame"
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test KevinLame`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
