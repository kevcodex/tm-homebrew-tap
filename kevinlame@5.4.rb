# Documentation: https://docs.brew.sh/Formula-Cookbook
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class KevinlameAT54 < Formula
  desc ""
  homepage "https://bitbucket.org/kevcodex"
  url "https://www.dropbox.com/s/k3h1yu8zpz5wrzk/5.4.tar.gz?dl=1"
  version "5.4"
  sha256 "40af630ba1daf8a6a50804c3b83ff00c2938551b483f1a0780194b10048f690d"
  # depends_on "cmake" => :build

  def install
    bin.install "KevinLame"
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test KevinLame`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
