# TeamMerge Homebrew Tap

Team merge taps.

***
## Getting Started ##
* run `brew tap kchen/tm-homebrew-private git@bitbucket.org:kevcodex/tm-homebrew-tap.git` to open this tap
* If using HTTPS, run brew tap kchen/tm-homebrew-private https://kevcodex@bitbucket.org/kevcodex/tm-homebrew-tap.git
* run `brew install kevinlame` to install kevinlame
* run `brew upgrade kevinlame` to update kevinlame
