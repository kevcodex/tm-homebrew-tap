# Documentation: https://docs.brew.sh/Formula-Cookbook
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class Swiftyscripts < Formula
    desc ""
    homepage ""
    url "https://www.dropbox.com/s/gz9rjpqfhhwx4vv/1.0.1.tar.gz?dl=1"
    version "1.0.1"
    sha256 "08647aeb4ed214df72f6e12eada279ada4ec29df805dc689147304c0669c3277"
    # depends_on "cmake" => :build
  
    def install
      bin.install "SwiftyScripts"
    end
  
    test do
      system "false"
    end
  end
  
