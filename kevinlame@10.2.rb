# Documentation: https://docs.brew.sh/Formula-Cookbook
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class KevinlameAT102 < Formula
  desc ""
  homepage "https://bitbucket.phunware.com/projects/SERVICES/repos/dev-team-merge-automator/browse"
  url "https://www.dropbox.com/s/jqd77z3e1nink5j/10.2.tar.gz?dl=1"
  version "10.2"
  sha256 "8a2afacc9a4b7b40efcff8dd263f1678240810b157dbcdb329337259a43eedd4"
  # depends_on "cmake" => :build

  def install
    bin.install "KevinLame"
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test KevinLame`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
