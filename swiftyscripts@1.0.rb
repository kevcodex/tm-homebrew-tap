# Documentation: https://docs.brew.sh/Formula-Cookbook
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class Swiftyscripts < Formula
    desc ""
    homepage ""
    url "https://www.dropbox.com/s/3ka7jo00tckjzsf/1.0.tar.gz?dl=1"
    version "1.0"
    sha256 "72aa66f8317a633c2412d1247797336fa046ededa7c6183fe48b804f613788c9"
    # depends_on "cmake" => :build
  
    def install
      bin.install "SwiftyScripts"
    end
  
    test do
      system "false"
    end
  end
  
